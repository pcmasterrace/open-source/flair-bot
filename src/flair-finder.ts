import * as snoowrap from 'snoowrap';
import { Log } from "./logger";

export class FlairFinder {

    private reddit: snoowrap;
    private subreddit: snoowrap.Subreddit;
    private log: Log;

    constructor(options: snoowrap.SnoowrapOptions, subreddit: string, log: Log) {
        this.log = log;
        this.reddit = new snoowrap(options);
        this.subreddit = this.reddit.getSubreddit(subreddit);
    }

    private getHot() {
        return this.subreddit.getHot();
    }

    public getUnflaired() {
        return this.getHot().filter((submission: snoowrap.Submission) => {
            return submission.link_flair_text === null;
        });
    }

    public getUnflairedLinks() {
        return this.getUnflaired().map((submission: snoowrap.Submission) => {
            submission.permalink = `http://reddit.com${submission.permalink}`;
            return submission;
        });
    }

}
