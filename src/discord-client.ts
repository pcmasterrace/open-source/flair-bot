import { Client } from "discord.io";
import { Submission } from "snoowrap";
import { Log } from "./logger";
export class DiscordConnection {

    private client: Client;
    private channelID: string;
    private log: Log;

    constructor(params: { token: string, autorun: boolean }, channelID: string, log: Log) {
        this.log = log;
        this.client = new Client(params);

        const thing = this;

        this.client.on('disconnect', () => {
            thing.client.connect();
        });

        this.client.on('ready', () => {
            thing.client.setPresence({
                game: {
                    name: "Browsing Reddit",
                    type: 0,
                },
                idle_since: -1,
            });
            thing.log.log("Bot ready!");
        });

        this.channelID = channelID;
    }
    public sendMessage(message: string, submission?: Submission) {
        if (!submission) {
            this.client.sendMessage({
                message,
                to: this.channelID,
            });
            return;
        }
        const log = this.log;
        this.client.sendMessage({
            embed: {
                author: {
                    icon_url: "https://cdn.discordapp.com/avatars/367814625103708160/45ad1054f834212c29462bbe658e750b.webp?size=256",
                    name: "Flair Finder",
                },
                color: 0x8b1fba,
                footer: {
                    icon_url: "https://cdn.discordapp.com/avatars/367814625103708160/45ad1054f834212c29462bbe658e750b.webp?size=256",
                    text: "Flair Finder",
                },
                timestamp: new Date(),
                title: submission.title,
                url: `https://reddit.com${submission.permalink}`
            },
            to: this.channelID,
        }, (error) => {
            if (error != null) {
                log.error(error.statusMessage);
            }
        });
    }

}
