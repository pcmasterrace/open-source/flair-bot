
import * as discord from "discord.io";
import { SnoowrapOptions } from "snoowrap";
import { DiscordConnection } from "./discord-client";
import { FlairFinder } from "./flair-finder";
import { Log } from "./logger";
import { RedisDriver } from "./redis-connection";

const logger = new Log(true);
const options: {snoowrap: SnoowrapOptions, subreddit: string} = require("../config.json").reddit;
const discordConfig = require("../config.json").discord;

const flairFinder = new FlairFinder(options.snoowrap, options.subreddit, logger);
const discordBot = new DiscordConnection(discordConfig, discordConfig.channel, logger);

const redis = new RedisDriver(logger);

function processPosts() {
    flairFinder.getUnflaired().map((redditSubmission) => {
        redis.beenChecked(redditSubmission.permalink).then((wasChecked) => {
            if(wasChecked) return;

            logger.post(`${redditSubmission.title}: http://reddit.com${redditSubmission.permalink}`);
            discordBot.sendMessage(`${redditSubmission.title}: ${redditSubmission.permalink}`, redditSubmission);
            redis.add(redditSubmission);            
        })

    });
}
processPosts();
setInterval(processPosts, 1000 * 60 * 3);
