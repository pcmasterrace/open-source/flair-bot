import * as bluebird from 'bluebird';
import * as redis from 'redis';
import * as snoowrap from 'snoowrap';
bluebird.promisifyAll(redis.RedisClient.prototype);
import { Log } from "./logger";


export class RedisDriver {

    private client: redis.RedisClient;
    private log: Log;
    constructor(log: Log){
        this.client = redis.createClient(process.env.REDIS_URL);
        this.log = log;

        this.client.on("error", log.error);
    }

    public beenChecked(permalink: string): Promise<boolean> {
        return this.client.getAsync(permalink).then((reply: string) => {
            return reply !== null;
        }).catch((err: string) => this.log.error);
    }

    public add(submission: snoowrap.Submission): void {
        this.beenChecked(submission.permalink).then(wasChecked => {
            if(wasChecked) return;
            
            this.client.hsetAsync(submission.permalink, "permalink", submission.permalink);
            this.client.hsetAsync(submission.permalink, "title", submission.title);
            this.client.hsetAsync(submission.permalink, "author", submission.author.name);
            
        })
    }

}
