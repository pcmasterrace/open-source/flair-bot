import * as chalk from 'chalk';

export class Log {
    private shouldLog: boolean;

    constructor(shouldLog: boolean) {
        this.shouldLog = shouldLog;
    }

    public log(message: string) {
        if (!this.shouldLog) return;
        console.log(`${chalk.green("Log:")} ${message}`);
    }

    public error(message: string) {
        if (!this.shouldLog) return;
        console.error(`${chalk.red("Error:")} ${message}`);
    }

    public info(message: string) {
        if (!this.shouldLog) return;
        console.info(`${chalk.blue("Info:")} ${message}`);
    }

    public warn(message: string) {
        if (!this.shouldLog) return;
        console.warn(`${chalk.yellow("Warn:")} ${message}`);
    }

    public post(title: string) {
        if (!this.shouldLog) return;
        console.log(`${chalk.cyan('Post Found')}: ${title}`);
    }
}
